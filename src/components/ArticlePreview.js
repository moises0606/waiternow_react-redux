import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { VTABLE_REMOVE_ORDERED, VTABLE_ADD_ORDERED, ORDER_SERVE } from '../constants/actionTypes';

const mapDispatchToProps = dispatch => ({
  onClickAdd: payload =>
    dispatch({ type: VTABLE_ADD_ORDERED, payload }),
  onClickRemove: (payload, vtable) =>
    dispatch({ type: VTABLE_REMOVE_ORDERED, payload, vtable }),
  onClickServe: (payload) =>
    dispatch({ type: ORDER_SERVE, payload }),
});

const ArticlePreview = props => {
  const article = props.tab == 'orders' || props.from == 'orders' ? props.article.article : props.article;

  const add = () => {
    props.onClickAdd(agent.Orders.create(props.vtable, article.slug));
  }

  const remove = () => {
    props.onClickRemove(agent.Orders.del(props.article.id_incremental), props.vtable);
  }

  const serve = () => {
    props.article.served = true;
    props.onClickServe(agent.Orders.update(props.article));
  }

  return (
    <div className="article-preview" >
      <div className="article-top">
        {props.tab == 'orders' ? <label className={props.article.served ? "served" : "notserved"}>{props.article.served ? "Served" : "For serving"}</label> : null}
        {props.from == 'orders' ? "Table: "+props.article.vtable.id_incremental:null }
      </div>
      <div className="article-body">
        <h4>{article.title}</h4>
        <p>{article.body}</p>
        <p>{article.price} €</p>
        <img className="article-img" src={article.img} alt="Image product"></img>
      </div>
      <div className="preview-bottom">
        {props.from == 'orders' ?
          <i className="signs" onClick={serve}>serve</i>
          :
          (
            props.tab == 'orders' ?
              <i className="signs" onClick={remove}>-</i>
              :
              <i className="signs" onClick={add}>+</i>
          )
        }
      </div>
    </div>
  );
}

export default connect(() => ({}), mapDispatchToProps)(ArticlePreview);
