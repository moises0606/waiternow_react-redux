import article from './reducers/article';
import auth from './reducers/auth';
import { combineReducers } from 'redux';
import common from './reducers/common';
import editor from './reducers/editor';
import home from './reducers/home';
import settings from './reducers/settings';
import order from './reducers/order';
import vTable from './reducers/vTable';
import vTableList from './reducers/vTableList';
import { routerReducer } from 'react-router-redux';

export default combineReducers({
  article,
  auth,
  common,
  editor,
  home,
  order,
  vTable,
  vTableList,
  settings,
  router: routerReducer
});
