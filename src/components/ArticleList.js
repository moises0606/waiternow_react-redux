import ArticlePreview from './ArticlePreview';
import React from 'react';

const ArticleList = props => {
  if (!props.articles) {
    return (
      <div className="article-preview">Loading...</div>
    );
  }

  if (props.articles.length === 0) {
    return (
      <div className="article-preview">
        No articles are here... yet.
      </div>
    );
  }
  let articles = props.articles;

  if (props.tab == 'orders')
    articles = props.articles.filter((article) => article.vtable.id_incremental == props.vtable)

  return (
    <div className="article-container">
      {
        articles.map((article, index) => {
          return (
            <ArticlePreview vtable={props.vtable} article={article} key={index} tab={props.tab} />
          );
        })
      }
    </div>
  );
};

export default ArticleList;
