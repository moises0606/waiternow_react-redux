import { VTABLE_PAGE_LOADED, VTABLE_PAGE_UNLOADED, VTABLE_REMOVE_ORDERED, VTABLE_ADD_ORDERED } from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case VTABLE_PAGE_LOADED:
    return {
        ...state,
        vTable: action.payload.filter((act) => act['virtualtable'])[0].virtualtable,
        articles: action.tab == 'orders' ? action.payload.filter((act) => act['orders'])[0].orders : action.payload.filter((act) => act['articles'])[0].articles,
        orders: action.payload.filter((act) => act['orders'])[0].orders,
        tab: action.tab
      };
    case VTABLE_REMOVE_ORDERED:
    case VTABLE_ADD_ORDERED:
    return {
      ...state
    }
    case VTABLE_PAGE_UNLOADED:
      return {};
    default:
      return state;
  }
};
