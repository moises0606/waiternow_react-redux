import { ORDER_PAGE_LOADED, ORDER_PAGE_UNLOADED, ORDER_SERVE } from '../constants/actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case ORDER_PAGE_LOADED:
            return {
                ...state,
                orders: action.payload == undefined ? [] : action.payload.orders.filter((order) => order.article.fromwhere == action.tab && order.served == false),
                tab: action.tab
            };
        case ORDER_PAGE_UNLOADED:
            return {};
        case ORDER_SERVE:
            return {
                ...state,
                orders: state.orders.filter((order) => order.id_incremental != action.payload.order.id_incremental)
            }
        default:
            return state;
    }
};
