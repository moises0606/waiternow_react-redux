import VTablesPreview from './vTablesPreview';
import React from 'react';

const VTablesList = props => {
  if (!props.vtables) {
    return (
      <div className="vtables-preview">Loading...</div>
    );
  }

  if (props.vtables.length === 0) {
    return (
      <div className="vtables-preview">
        No tables opened yet...
      </div>
    );
  }else 
    props.vtables.sort(function(a, b){return a.id_incremental-b.id_incremental})

  return (
    <div className="vtables-list">
      {
        props.vtablesCount !== 0 ?  
        props.vtables.map((vtable) => {
          return (
            <VTablesPreview numtable={vtable.id_incremental} vtable={vtable} key={vtable.id_incremental} />
          );
        }):null
      
      }

    </div>
  );
};

export default VTablesList;
