import React from 'react';
import { Link } from 'react-router-dom';

const VTablesCreate = props => {
  if (props.token) {
    return (
      <li className="nav-item">
        <Link
          to={`/vTables/`}>
          <button className="btn btn-outline btn-sm create" /*onClick={create}*/>
            <span className="iconify" data-icon="gridicons:create" data-inline="false"></span> Open Table
          </button>
        </Link>
      </li>
    );
  }
  return null;
};

export default VTablesCreate;