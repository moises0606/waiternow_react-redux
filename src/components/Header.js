import React from 'react';
import { Link } from 'react-router-dom';

const LoggedOutView = props => {
  if (!props.currentUser) {
    
    return (
      <ul className="nav-menu">

        <li className="nav-item">
          <Link to="/login" className="nav-link">
            Sign in
          </Link>
        </li>

      </ul>
    );
  }
  return null;
};

const LoggedInView = props => {
  if (props.currentUser) {
    return (
      <ul className="nav-menu">

        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/order" className="nav-link">
            Orders
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/editor" className="nav-link">
            <i className="ion-compose"></i>&nbsp;New Product
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/settings" className="nav-link">
            <i className="ion-gear-a"></i>&nbsp;Settings
          </Link>
        </li>

        <li className="nav-item">
          <Link
            to={`/`}
            className="nav-link">
            {props.currentUser.username}
          </Link>
        </li>

      </ul>
    );
  }

  return null;
};

class Header extends React.Component {
  render() {
    return (
      <header id="header" id="home">
        <div className="container main-menu">
          <div className="header align-items-center justify-content-between d-flex">
            <div id="logo">
              <Link to="/" className="navbar-brand">
                <img className="logo" alt="logo empresa" src="/img/logo.png"></img>
                {this.props.appName.toLowerCase()}
              </Link>
            </div>
            <nav id="nav-menu-container">
              <LoggedOutView currentUser={this.props.currentUser} />

              <LoggedInView currentUser={this.props.currentUser} />
            </nav>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
