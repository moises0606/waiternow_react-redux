import {
    SET_PAGE,
    HOME_PAGE_LOADED,
    HOME_PAGE_UNLOADED,
    CLOSE_TABLE
  } from '../constants/actionTypes';

  export default (state = {}, action) => {
    switch (action.type) {
      case SET_PAGE:
        return {
          ...state,
          vtables: action.payload.virtualtables,
          vtablesCount: action.payload.virtualtablesCount,
          currentPage: action.page
        };
      case HOME_PAGE_LOADED:
        return {
          ...state,
          pager: action.pager,
          vtables: action.payload.virtualtables,
          vtablesCount: action.payload.virtualtablesCount,
          currentPage: 0,
          tab: action.tab
        };
      case CLOSE_TABLE:
          let tableclosed = action.payload.virtualtable.id_incremental;
          state.vtables = state.vtables.filter((vtable) => vtable.id_incremental != tableclosed);
          state.vtablesCount--;
        
          return {        
            ...state,
            vtableclosed: action.payload.virtualtable
            };
      case HOME_PAGE_UNLOADED:
        return {};
      default:
        return state;
    }
  };