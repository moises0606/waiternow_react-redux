import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { CLOSE_TABLE } from '../constants/actionTypes';
import { Link } from 'react-router-dom';


const mapStateToProps = state => ({
  ...state.vTableList,
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({
  onClick: payload =>
    dispatch({ type: CLOSE_TABLE, payload }),
});

const VTablePreview = props => {
  const vtable = props.vtable;

  const close = () => {
    vtable.active = false;
    props.onClick(agent.vTables.update(vtable));
  }

  props.vtableclosed == undefined ? props = { ...props, vtableclosed: { id_incremental: 0 } } : null;

  if (props.vtableclosed.id_incremental == props.vtable.id_incremental)
    return null;
  else {
    return (
      <div className="vtables-preview">
        <div className="vtable-top">
          <Link
            to={`/vTables/${props.vtable.id_incremental}`} aria-label="Open table">
            <span className="nav-item">Table {props.numtable}</span>
          </Link>
          <button className="btn btn-outline-danger btn-sm" onClick={close}>
            Close Table
          </button>
        </div>
        <Link
          to={`/vTables/${props.vtable.id_incremental}`} aria-label="Open table">
          <div className="vtable-img"></div>
        </Link>
      </div>
    )
  };
}

export default connect(() => mapStateToProps, mapDispatchToProps)(VTablePreview);