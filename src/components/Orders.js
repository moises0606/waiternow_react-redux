//import MainView from './MainView';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import ArticlePreview from './ArticlePreview';
import {
  ORDER_PAGE_LOADED,
  ORDER_PAGE_UNLOADED,
} from '../constants/actionTypes';

const Bar = props => {
  const clickHandler = ev => {
    ev.preventDefault();
    props.onTabClick(agent.Orders.all(), 'Bar');
  };
  return (
    <li className="nav-item">
      <a
        className={props.tab === 'Bar' ? 'nav-links active' : 'nav-links'}
        onClick={clickHandler}>
        Bar
      </a>
    </li>
  );
};

const Kitchen = props => {
  const clickHandler = ev => {
    ev.preventDefault();
    props.onTabClick(agent.Orders.all(), 'Kitchen');
  };
  return (
    <li className="nav-item">
      <a
        className={props.tab === 'Kitchen' ? 'nav-links active' : 'nav-links'}
        onClick={clickHandler}>
        Kitchen
      </a>
    </li>
  );
};

const mapStateToProps = state => ({
  ...state.order,
  appName: state.common.appName,
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, tab) =>
    dispatch({ type: ORDER_PAGE_LOADED, tab, payload }),
  onUnload: () =>
    dispatch({ type: ORDER_PAGE_UNLOADED })
});


class Order extends React.Component {
  componentWillMount() {
    this.props.onLoad(agent.Orders.all(), 'Bar');
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  navigation() {

    return (
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">
          <Bar onTabClick={this.props.onLoad} currentUser={this.props.currentUser} match={this.props.match} tab={this.props.tab} />
          <Kitchen onTabClick={this.props.onLoad} currentUser={this.props.currentUser} match={this.props.match} tab={this.props.tab} />
        </ul>
      </div>
    )
  }

  render() {
    if (this.props.orders == undefined || this.props.orders.length == 0) {
      return (
        <div className="vtable-page">
          {this.navigation()}
          No orders yet...
        </div>
      );
    } else
      this.props.orders.sort(function (a, b) { return a.id_incremental - b.id_incremental })

    return (
      <div className="orders-container">
        {this.navigation()}
        <div className="article-container">
          {
            this.props.orders.map((order, index) => {
              console.log(order)
              return (
                <ArticlePreview article={order} key={index} from="orders" />
              );
            })
          }
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Order);
