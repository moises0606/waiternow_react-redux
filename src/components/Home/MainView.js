import React from 'react';
import { connect } from 'react-redux';
import { CHANGE_TAB } from '../../constants/actionTypes';
import VTablesList from '../vTablesList';
import VTablesCreate from '../vTablesCreate';


const mapStateToProps = state => ({
  ...state.vTableList,
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({
  onTabClick: (tab, pager, payload) => dispatch({ type: CHANGE_TAB, tab, pager, payload })
});

const MainView = props => {
  return (
    <div className="main-container">
      <div className="main-top">
        <ul className="nav nav-pills outline-active">

          <VTablesCreate 
            token={props.token}
          />

        </ul>
      </div>

      <VTablesList
        vtables={props.vtables}/>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
