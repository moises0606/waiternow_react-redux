import React from 'react';
import agent from '../../agent';
import ArticleList from '../ArticleList';
import VTablesActions from './vTablesActions';
import { connect } from 'react-redux';
import { VTABLE_PAGE_LOADED, VTABLE_PAGE_UNLOADED } from '../../constants/actionTypes';


const Ordered = props => {
  const clickHandler = ev => {
    ev.preventDefault();
    props.onTabClick(
      Promise.all([
        agent.vTables.get(props.match.params.id),
        agent.Articles.byAuthor(props.currentUser.username),
        agent.Orders.all()
      ]), 'orders'
    );
  };
  return (
    <li className="nav-item">
      <a
        className={ props.tab === 'orders' ? 'nav-links active' : 'nav-links' }
        onClick={clickHandler}>
        Ordered
      </a>
    </li>
  );
};

const Articles = props => {
  const clickHandler = ev => {
    ev.preventDefault();
    props.onTabClick(
      Promise.all([
        agent.vTables.get(props.match.params.id),
        agent.Articles.byAuthor(props.currentUser.username),
        agent.Orders.all()
      ]), 'products'
    );
  };
  return (
    <li className="nav-item">
      <a
        className={ props.tab === 'products' ? 'nav-links active' : 'nav-links' }
        onClick={clickHandler}>
        Products
      </a>
    </li>
  );
};

const mapStateToProps = state => ({
  ...state.vTable,
  currentUser: state.common.currentUser,
  vTable: state.vTable
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, tab) =>
    dispatch({ type: VTABLE_PAGE_LOADED, payload:payload, tab }),
  onUnload: () =>
    dispatch({ type: VTABLE_PAGE_UNLOADED })
});

class VTables extends React.Component {
  componentWillMount() {
    const choose = this.props.match.params.id ? agent.vTables.get(this.props.match.params.id) : agent.vTables.create();
    

    this.props.onLoad(Promise.all([
      choose,
      agent.Articles.byAuthor(this.props.currentUser.username),
      agent.Orders.all()
    ]), 'orders');
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  navigation() {

    return (
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">
        <Articles onTabClick={this.props.onLoad} currentUser={this.props.currentUser} match={this.props.match} tab={this.props.tab}/>
        <Ordered onTabClick={this.props.onLoad} currentUser={this.props.currentUser} match={this.props.match} tab={this.props.tab}/>      
        </ul>
      </div>
    )
  }

  render() {
    if (!this.props.vTable.vTable) {
      return (
        <div className="vtable-page">Loading...</div>
      );
    }

    let vTable = this.props.vTable.vTable;
    let articles = this.props.vTable.articles;

    const canControl = this.props.currentUser && this.props.currentUser.username === vTable.author.username;

    return (
      <div className="vtable-page">

        <div className="banner">
          <div className="container">

            <h1>Table {vTable.id_incremental}</h1>

            <VTablesActions orders={this.props.orders} canControl={canControl} vtable={vTable} />
          </div>
        </div>


        {this.navigation()}

        <div className="container-articles">

              <ArticleList articles={articles} vtable={vTable.id_incremental} tab={this.props.tab}/>

        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VTables);
