import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

//const API_ROOT = 'http://localhost:8000/api';
const API_ROOT = 'https://waiternow.herokuapp.com/api';

const encode = encodeURIComponent;
const responseBody = res => res.body;

let token = null;
const tokenPlugin = req => {
  if (token) {
    req.set('authorization', `Token ${token}`);
  }
}

const requests = {
  del: url =>
    superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  get: url =>
    superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
  put: (url, body) =>
    superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
  post: (url, body) =>
    superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Auth = {
  current: () =>
    requests.get('/user'),
  login: (email, password) =>
    requests.post('/users/login', { user: { email, password } }),
  register: (username, email, password) =>
    requests.post('/users', { user: { username, email, password } }),
  save: user =>
    requests.put('/user', { user })
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug = article => Object.assign({}, article, { slug: undefined })
const Articles = {
  all: () =>
    requests.get(`/articles`),
  byAuthor: (author, page) =>
    requests.get(`/articles?author=${encode(author)}`),
  del: slug =>
    requests.del(`/articles/${slug}`),
  favorite: slug =>
    requests.post(`/articles/${slug}/favorite`),
  favoritedBy: (author, page) =>
    requests.get(`/articles?favorited=${encode(author)}&${limit(5, page)}`),
  feed: () =>
    requests.get('/articles/feed/'),//?limit=10&offset=0
  get: slug =>
    requests.get(`/articles/${slug}`),
  unfavorite: slug =>
    requests.del(`/articles/${slug}/favorite`),
  update: article =>
    requests.put(`/articles/${article.slug}`, { article: omitSlug(article) }),
  create: article =>
    requests.post('/articles', { article })
};

const omitId = vtable => Object.assign({}, vtable, { id_incremental: undefined })
const vTables = {
  all: page =>
    requests.get(`/virtualtables?${limit(10, page)}`),
  feed: () =>
    requests.get('/virtualtables/vt/feed?limit=100&offset=0'),
  get: id =>
    requests.get(`/virtualtables/${id}`),
  update: vtables =>
    requests.put(`/virtualtables/${vtables.id_incremental}`, { virtualtable: omitId(vtables) }),
  create: () =>
    requests.post('/virtualtables'),
  del: id =>
    requests.del(`/virtualtables/${id}`)
};

const Orders = {
  all: () =>
    requests.get(`/virtualtables/orders/`),
  get: (order) =>
    requests.get(`/virtualtables/orders/${order}`),
  update: order => 
    requests.put(`/virtualtables/order/${order.id_incremental}`, { order: omitId(order) }),
  create: (vtable, article) =>
    requests.post(`/virtualtables/orders/`, { vtable: vtable, article: article }),
  del: id =>
    requests.del(`/virtualtables/orders/delete/${id}`)

};


export default {
  Articles,
  vTables,
  Orders,
  Auth,
  setToken: _token => { token = _token; }
};
