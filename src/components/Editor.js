import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import {
  EDITOR_PAGE_LOADED,
  ARTICLE_SUBMITTED,
  EDITOR_PAGE_UNLOADED,
  UPDATE_FIELD_EDITOR
} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.editor
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload =>
    dispatch({ type: EDITOR_PAGE_LOADED, payload }),
  onSubmit: payload =>
    dispatch({ type: ARTICLE_SUBMITTED, payload }),
  onUnload: payload =>
    dispatch({ type: EDITOR_PAGE_UNLOADED }),
  onUpdateField: (key, value) =>
    dispatch({ type: UPDATE_FIELD_EDITOR, key, value })
});

class Editor extends React.Component {
  constructor() {
    super();

    const updateFieldEvent =
      key => ev => {
        key == 'img' ? this.getImage(key) : this.props.onUpdateField(key, ev.target.value);
      };

    this.changeTitle = updateFieldEvent('title');
    this.changeBody = updateFieldEvent('body');
    this.changeImg = updateFieldEvent('img');
    this.changeFromwhere = updateFieldEvent('fromwhere');
    this.changePrice = updateFieldEvent('price');

    this.submitForm = ev => {
      ev.preventDefault();
      const article = {
        title: this.props.title,
        body: this.props.body,
        img: this.props.img,
        fromwhere: this.props.fromwhere,
        price: this.props.price
      };

      const slug = { slug: this.props.articleSlug };
      const promise = this.props.articleSlug ?
        agent.Articles.update(Object.assign(article, slug)) :
        agent.Articles.create(article);

      this.props.onSubmit(promise);
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.match.params.slug !== nextProps.match.params.slug) {
      if (nextProps.match.params.slug) {
        this.props.onUnload();
        return this.props.onLoad(agent.Articles.get(this.props.match.params.slug));
      }
      this.props.onLoad(null);
    }
  }

  componentWillMount() {
    if (this.props.match.params.slug) {
      return this.props.onLoad(agent.Articles.get(this.props.match.params.slug));
    }
    this.props.onLoad(null);
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  getImage(key) {
    var file = this.refs.file.files[0];
    var reader = new FileReader();

    reader.onloadend = () => this.props.onUpdateField(key, reader.result);
    
    reader.readAsDataURL(file);
  }

  render() {
    return (
      <div className="editor-page">
        <div className="container page">
          <div className="row">
            <div className="col-md-10 offset-md-1 col-xs-12">

              <ListErrors errors={this.props.errors}></ListErrors>

              <form>
                <fieldset>

                  <fieldset className="form-group">
                    <input
                      aria-label="titulo"
                      className="form-control form-control-lg"
                      type="text"
                      placeholder="Product Title"
                      value={this.props.title}
                      onChange={this.changeTitle} />
                  </fieldset>

                  <fieldset className="form-group">
                    <input
                      aria-label="Subir imagen"
                      className="form-control"
                      ref="file"
                      type="file"
                      placeholder="Set image to product"
                      onChange={this.changeImg}>
                    </input>
                    <img className="updateImg" src={this.props.img}  alt="Imagen subida"></img>
                  </fieldset>

                  <fieldset>
                    <label className="typeProduct"><input
                      className="form-control form-control-lg"
                      type="radio"
                      name="fromwhere"
                      value="Bar"
                      onChange={this.changeFromwhere} />Bar</label>
                    <label className="typeProduct"><input
                      className="form-control form-control-lg"
                      type="radio"
                      name="fromwhere"
                      value="Kitchen"
                      onChange={this.changeFromwhere} />Kitchen</label>
                  </fieldset>

                  <fieldset>
                    <input
                      aria-label="precio"
                      className="form-control form-control-lg"
                      type="number"
                      placeholder="Price"
                      value={this.props.price}
                      onChange={this.changePrice}
                      min="0"
                    />
                  </fieldset>

                  <fieldset className="form-group">
                    <textarea
                      aria-label="Añadir descripcion de producto"
                      className="form-control"
                      rows="8"
                      placeholder="Describe the product"
                      value={this.props.body}
                      onChange={this.changeBody}>
                    </textarea>
                  </fieldset>

                  <button
                    aria-label="enviar"
                    className="btn btn-lg pull-xs-right btn-primary"
                    type="button"
                    disabled={this.props.inProgress}
                    onClick={this.submitForm}>
                    Publish Article
                  </button>

                </fieldset>
              </form>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor);
