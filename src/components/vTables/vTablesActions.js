import React from 'react';
import agent from '../../agent';
import { connect } from 'react-redux';
import { CLOSE_TABLE_GOTOHOME } from '../../constants/actionTypes';

const mapDispatchToProps = dispatch => ({
  onClick: payload =>
    dispatch({ type: CLOSE_TABLE_GOTOHOME, payload }),
});

const VTablesActions = props => {
  const vtable = props.vtable;
  const close = () => {
    vtable.active = false;
    props.onClick(agent.vTables.update(vtable));
  }
  let price = 0;
  props.orders.map((order) => order.vtable.id_incremental == props.vtable.id_incremental ? price += parseFloat(order.article.price):null);

  if (props.canControl) {
    return (
      <div className="vtable-actions">
        <h3>Total {price} €</h3>
        <span>
          <button className="btn btn-outline-danger btn-sm" onClick={close}>
            Close vtable
        </button>
        </span>
      </div>
    );
  }

  return (
    <span>
    </span>
  );
};

export default connect(() => ({}), mapDispatchToProps)(VTablesActions);
